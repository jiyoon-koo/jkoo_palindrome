require "jkoo_palindrome/version"

module JkooPalindrome

  def palindrome?
    processed_content == processed_content.reverse
  end

  private
    def processed_content
      self.to_s.scan(/[a-z\d]/i).join.downcase
    end
end


class String
  include JkooPalindrome
end

class Integer
  include JkooPalindrome
end
